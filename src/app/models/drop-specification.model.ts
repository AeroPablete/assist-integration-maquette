export interface DropSpecification {
    weight: number
    pallets: number
    linearFeet: number
}