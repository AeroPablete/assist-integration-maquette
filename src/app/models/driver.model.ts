export interface Driver {
    name: string
    isAssigned: boolean
}