import { Client } from "./client.model"
import { DropSpecification } from "./drop-specification.model"

export interface Drop {
    amount: number
    deliveryClient: Client
    displayReference: string,
    dropSpecification: DropSpecification
    pickupClient: Client
}