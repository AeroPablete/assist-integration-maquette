export interface Location {
    streetNumber: string
    postalCode: string
    city: string
    stateOrProvince: string
    country: string
    timeZone: string
}