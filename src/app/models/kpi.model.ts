export interface KPI {
    label: string
    value: string
}