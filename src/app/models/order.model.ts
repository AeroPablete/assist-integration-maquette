import { Drop } from "./drop.model";

export interface Order {
    id: number,
    displayReference: string,
    isOutBound: boolean,
    isPicked: boolean,
    drops: Drop[]
}