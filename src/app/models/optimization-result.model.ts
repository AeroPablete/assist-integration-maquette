import { Driver } from "./driver.model";
import { Order } from "./order.model";

export interface OptimisationResult {
    drivers: Driver[]
    unassignedDrivers: Driver[]
    unassignedOrders: Order[]
}