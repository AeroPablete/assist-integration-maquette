import { Location } from "./location.model";

export interface Client {
    id: number
    clientName: string
    displayName: string
    dropSwitchMode: string
    location: Location
}