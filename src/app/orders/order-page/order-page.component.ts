import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Order } from 'src/app/models/order.model';
import { OrderService } from 'src/app/services/order.service';
import { OrderFilters } from '../order-menu/order-menu.component';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.scss']
})
export class OrderPageComponent implements OnInit, OnDestroy {

  /** Data Subscription */
  private _dataSubscription!: Subscription

  /** Orders By City Dictionary */
  ordersByCity: { [city: string]: Order[] } = {}

  /** City List */
  cityList: { label: string, code: string }[] = []

  /**
   * 
   * @param _orderService 
   */
  constructor(
    private _orderService: OrderService) { }

  /**
   * 
   */
  ngOnInit(): void {
    this._dataSubscription = this._orderService
      .getData()
      .subscribe(data => {
        this.ordersByCity = data.deliveries
        this._updateCityList(data.deliveries)
      })
  }

  /**
   * 
   */
  ngOnDestroy(): void {
    if (this._dataSubscription) this._dataSubscription.unsubscribe()
  }

  /**
   * 
   * @param filters 
   */
  onFiltersChange(filters: any) {
    const data = filters.typeFilter === 'deliveries' ?
      this._orderService.getDataSnapshot().deliveries : this._orderService.getDataSnapshot().pickups

    this._updateCityList(data)
    this._filterOrderList(data, filters)
  }

  /**
   * 
   * @param data 
   */
  private _updateCityList(data: { [city: string]: Order[] }) {
    this.cityList = Object.keys(data)
      .sort()
      .map(s => ({ label: s.replace(/\./g, ', '), code: s }))
  }

  /**
   * 
   */
  private _filterOrderList(data: { [city: string]: Order[] }, filters: OrderFilters) {
    // Filter Orders by City
    data = this._filterOrdersByCity(data, filters.cityFilter)

    // Filter Orders By Ref
    data = this._filterOrdersByRef(data, filters.orderRefFilter)

    // Filter Orders By client
    data = this._filterOrdersByClient(data, filters.clientNameFilter, filters.typeFilter)

    // Show orders
    this.ordersByCity = data
  }

  /**
   * 
   * @param data 
   * @param cityFilter 
   * @returns 
   */
  private _filterOrdersByCity(data: { [city: string]: Order[] }, cityFilter: string) {
    if (!cityFilter) return data

    if (Object.keys(data).includes(cityFilter)) {
      return { [cityFilter]: data[cityFilter] }
    }
    else {
      return {}
    }
  }

  /**
   * 
   * @param data 
   * @param refFilter 
   */
  private _filterOrdersByRef(data: { [city: string]: Order[] }, refFilter: string) {
    if (!refFilter) return data

    const copy = { ...data }
    Object.keys(data).forEach(key => {
      copy[key] = copy[key].filter(o => o.displayReference.toLowerCase().includes(refFilter.toLowerCase()))
    })

    return copy
  }

  /**
   * 
   * @param data 
   * @param refFilter 
   */
   private _filterOrdersByClient(data: { [city: string]: Order[] }, clientFilter: string, typeFilter: string) {
    if (!clientFilter) return data

    const copy = { ...data }
    Object.keys(data).forEach(key => {
      copy[key] = copy[key].filter(o => {
        if (typeFilter === 'deliveries') {
          const aux = o.drops.map(d => d.deliveryClient.clientName.toLowerCase()).join(',')
          return aux.includes(clientFilter.toLowerCase())
        }
        else {
          const aux = o.drops.map(d => d.pickupClient.clientName.toLowerCase()).join(',')
          return aux.includes(clientFilter.toLowerCase())
        }
      })
    })

    return copy
  }

}
