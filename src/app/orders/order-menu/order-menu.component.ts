import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { OrderService } from 'src/app/services/order.service';

export interface OrderFilters {
  cityFilter: string
  clientNameFilter: string
  orderRefFilter: string
  typeFilter: string
}

@Component({
  selector: 'app-order-menu',
  templateUrl: './order-menu.component.html',
  styleUrls: ['./order-menu.component.scss']
})
export class OrderMenuComponent implements OnInit {
  
  /** ... */
  @Input('cityList') cityList: { label: string, code: string }[] = []

  /** Order Filter */
  @Output('filterItems') filterItems = new EventEmitter<OrderFilters>()

  /** Form Definition */
  form = new FormGroup({
    main: new FormGroup({
      typeFilter: new FormControl('deliveries')
    }),
    sub: new FormGroup({
      cityFilter: new FormControl(''),
      clientNameFilter: new FormControl(''),
      orderRefFilter: new FormControl(''),
    })
  })

  /**
   * 
   */
  ngOnInit(): void {
    this.form.get('main')!
      .valueChanges
      .subscribe(_ => {
        this.form.get('sub')!.setValue({
          cityFilter: '',
          clientNameFilter: '',
          orderRefFilter: '',
        })
      })

    this.form.get('sub')!
      .valueChanges
      .subscribe(_ => {
        const valueToEmit = {...this.form.get('main')!.value, ...this.form.get('sub')!.value}
        this.filterItems.emit(valueToEmit)
      })
  }
}
