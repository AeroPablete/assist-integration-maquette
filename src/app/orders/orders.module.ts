import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { OrderPageComponent } from './order-page/order-page.component';
import { OrderMenuComponent } from './order-menu/order-menu.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    OrderMenuComponent,
    OrderPageComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    OrderPageComponent
  ]
})
export class OrdersModule { }
