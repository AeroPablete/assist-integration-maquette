import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: InputComponent
    }
  ]
})
export class InputComponent implements ControlValueAccessor {

  /** Input Placeholder */
  @Input('placeholder') placeholder: string = ''

  /** ... */
  inputValue: string = ''

  /**
   * 
   * @param obj 
   */
  writeValue(value: string): void {
    this.inputValue = value
  }

  /**
   * Register OnChange
   * @param fn 
   */
  _onchange = (value: string) => {}
  registerOnChange(fn: any): void {
    this._onchange = fn
  }

  /**
   * Register OnTouched
   * @param fn 
   */
  _onTouch = () => {}
  registerOnTouched(fn: any): void {
    this._onTouch = fn
  }

  /**
   * 
   * @param s 
   */
  setInputValue(s: string) {
    this.inputValue = s
    this._onchange(this.inputValue)
  }

  /**
   * 
   */
  resetInputValue() {
    this.inputValue = ''
    this._onchange(this.inputValue)
  }
}
