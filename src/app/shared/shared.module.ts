import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MainButtonComponent } from './main-button/main-button.component';
import { GlobalKpiComponent } from './global-kpi/global-kpi.component';
import { CardComponent } from './card/card.component';
import { SwitchComponent } from './switch/switch.component';
import { ComboComponent } from './combo/combo.component';
import { InputComponent } from './input/input.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { BadgeComponent } from './badge/badge.component';
import { TabsComponent } from './tabs/tabs.component';


@NgModule({
  declarations: [
    BadgeComponent,
    CardComponent,
    ComboComponent,
    GlobalKpiComponent,
    HeaderComponent,
    InputComponent,
    MainButtonComponent,
    SpinnerComponent,
    SwitchComponent,
    TabsComponent     
  ],
  imports: [
    CommonModule
  ], 
  exports: [
    BadgeComponent,
    CardComponent,
    ComboComponent,
    GlobalKpiComponent,
    HeaderComponent,
    InputComponent,
    MainButtonComponent,
    SpinnerComponent,
    SwitchComponent,
    TabsComponent
  ]
})
export class SharedModule { }
