import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-global-kpi',
  templateUrl: './global-kpi.component.html',
  styleUrls: ['./global-kpi.component.scss']
})
export class GlobalKpiComponent {

  /** KPI Label */
  @Input('label') label = ''

  /** KPI Value */
  @Input('value') value = ''
}
