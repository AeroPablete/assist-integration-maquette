import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

interface TabItem {
  label: string
  value: string
}

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TabsComponent
    }
  ]
})
export class TabsComponent implements ControlValueAccessor {

  /** Tab Items */
  @Input('tabs') tabs: TabItem[] = []

  /** Selected Tab */
  selectedTab?: TabItem

  /** Write Value */
  writeValue(value: string): void {
    this.selectedTab = this.tabs.find(t => t.value === value)
  }

  /** Register OnChange */
  _onChange = (value: string) => {}
  registerOnChange(fn: any): void {
    this._onChange = fn
  }

  /** Register OnTouched */
  _onTouch = () => {}
  registerOnTouched(fn: any): void {
    this._onTouch = fn
  }

  /**
   * 
   * @param item 
   */
  selectTab(item: TabItem) {
    this.selectedTab = item
    this._onChange(this.selectedTab.value)
  }
}
