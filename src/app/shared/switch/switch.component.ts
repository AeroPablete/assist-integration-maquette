import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SwitchComponent
    }
  ]
})
export class SwitchComponent implements ControlValueAccessor {  

  /** Label */
  @Input('label') label = 'Label'

  /** Is Checked Flag */
  isChecked = false

  /** ... */
  _onChange = (value: boolean) => {}

  /** ... */
  _onTouched = () => {}

  /**
   * 
   * @param value 
   */
  writeValue(value: boolean): void {
    this.isChecked = value
  }

  /**
   * 
   * @param fn 
   */
  registerOnChange(fn: any): void {
    this._onChange = fn
  }

  /**
   * 
   * @param fn 
   */
  registerOnTouched(fn: any): void {
    this._onTouched = fn
  }

  /**
   * 
   */
  onToggle() {
    this.isChecked = !this.isChecked
    this._onChange(this.isChecked)
  }
}
