import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

interface ComboOption {
  label: string
  code: string
}

@Component({
  selector: 'app-combo',
  templateUrl: './combo.component.html',
  styleUrls: ['./combo.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: ComboComponent
    }
  ]
})
export class ComboComponent implements ControlValueAccessor {

  /** List of Options */
  @Input('optionList') optionList: ComboOption[] = []

  /** Input placeholder */
  @Input('placeholder') placeholder: string = ''

  /** Selected Option */
  optionSelected?: ComboOption

  /** Flag pour controler si la liste est affichée */
  isListOfOptionsVisible = false

  /** ... */
  query: string = ''

  /**
   * 
   * @param obj 
   */
  writeValue(value: string): void {
    this.optionSelected = this.optionList.find(o => o.code === value)
  }

  /**
   * 
   * @param fn 
   */
  _onChange = (value: string) => {}
  registerOnChange(fn: any): void {
    this._onChange = fn
  }

  /**
   * Register OnTouched
   * @param fn 
   */
  _onTouch = () => {}  
  registerOnTouched(fn: any): void {
    this._onTouch = fn
  }

  /**
   * 
   * @param s 
   */
  setQuery(s: string) {
    this.query = s.trim()
  }

  /**
   * 
   * @param option 
   */
  selectOption(option: ComboOption) {
    this.optionSelected = option
    this.hideListOfOptions()
    this._onChange(this.optionSelected.code)
  }

  /**
   * 
   */
  resetSelection() {
    this.optionSelected = undefined
    this._onChange('')
  }

  /**
   * 
   */
  filterListOfOptions() {
    if (!this.query) return [...this.optionList]

    return this.optionList.filter(o => o.label.toLowerCase().includes(this.query.toLowerCase()))
  }

  /**
   * 
   */
  showListOfOptions() {
    this.isListOfOptionsVisible = true
  }

  /**
   * 
   */
  hideListOfOptions() {
    this.isListOfOptionsVisible = false
  }
}
