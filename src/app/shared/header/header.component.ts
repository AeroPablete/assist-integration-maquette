import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { KpiService } from 'src/app/services/kpi.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  /**
   * 
   * @param _dataService 
   */
  constructor(
    public dataService: DataService,
    public kpiService: KpiService) {}

  /**
   * 
   */
  optimize() {
    this.dataService.optimizeRoutes()
  }

}
