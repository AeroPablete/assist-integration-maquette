import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-driver-menu',
  templateUrl: './driver-menu.component.html',
  styleUrls: ['./driver-menu.component.scss']
})
export class DriverMenuComponent implements OnInit  {

  /** Driver Query */
  @Output('filterItems') filterItems = new EventEmitter<{query: string, assigned: boolean}>()

  /** Expands Items */
  @Output('expandItems') expandItems = new EventEmitter<boolean>()

  /** ... */
  form = new FormGroup({
    driverAssigned: new FormControl(true),
    expandItems: new FormControl(false),
    driverName: new FormControl('')
  })

  /**
   * 
   */
  ngOnInit(): void {
    this.form.get('driverName')!
      .valueChanges
      .subscribe(val => this.filterItems.emit({query: val, assigned: this.form.get('driverAssigned')!.value}))
    
    this.form.get('driverAssigned')!
      .valueChanges
      .subscribe(val => this.filterItems.emit({query: this.form.get('driverName')!.value, assigned: val}))
    
    this.form.get('expandItems')!
      .valueChanges
      .subscribe(val => this.expandItems.emit(val))
  }
}
