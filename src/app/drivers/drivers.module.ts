import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DriverPageComponent } from './driver-page/driver-page.component';
import { DriverMenuComponent } from './driver-menu/driver-menu.component';
import { SharedModule } from '../shared/shared.module';
import { DriverItemComponent } from './driver-item/driver-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DriverListComponent } from './driver-list/driver-list.component';
import { DriverSummaryComponent } from './driver-item/driver-summary/driver-summary.component';
import { DriverCurrentActivityComponent } from './driver-item/driver-current-activity/driver-current-activity.component';
import { DriverNextActivityListComponent } from './driver-item/driver-next-activity-list/driver-next-activity-list.component';
import { DriverNextActivityItemComponent } from './driver-item/driver-next-activity-item/driver-next-activity-item.component';


@NgModule({
  declarations: [
    DriverCurrentActivityComponent,
    DriverItemComponent,
    DriverListComponent,
    DriverMenuComponent,
    DriverNextActivityItemComponent,
    DriverNextActivityListComponent,
    DriverPageComponent,
    DriverSummaryComponent    
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    DriverPageComponent
  ]
})
export class DriversModule { }
