import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Driver } from 'src/app/models/driver.model';
import { DriverService } from 'src/app/services/driver.service';

@Component({
  selector: 'app-driver-page',
  templateUrl: './driver-page.component.html',
  styleUrls: ['./driver-page.component.scss']
})
export class DriverPageComponent implements OnInit, OnDestroy {

  /** Data Subscription */
  private _dataSubscription!: Subscription

  /** Driver List */
  driverList: Driver[] = []

  /** Expand Items Flag */
  expandItems!: boolean

  /**
   * 
   * @param _driverService 
   */
  constructor(
    private _driverService: DriverService) { }

  /**
   * 
   */
  ngOnInit(): void {
    this._dataSubscription = this._driverService
      .getData()
      .subscribe(data => {
        this.driverList = data
          .filter(this._filterDriverByAssigned(true))
          .filter(this._filterDriverByName(''))
      })
  }

  /**
   * 
   */
  ngOnDestroy(): void {
    if (this._dataSubscription) this._dataSubscription.unsubscribe()
  }

  /**
   * 
   * @param query 
   */
  filterDriverList(filters: {query: string, assigned: boolean}) {
    this.driverList = this._driverService.getDataSnapshot()
      .filter(this._filterDriverByAssigned(filters.assigned))
      .filter(this._filterDriverByName(filters.query))
  }

  _filterDriverByName(filter: string) {
    return (driver: Driver) => {
      if (!filter) return true
      return driver.name?.toLowerCase().includes(filter.toLowerCase())
    }
  }

  _filterDriverByAssigned(filter: boolean) {
    return (driver: Driver) => driver.isAssigned === filter    
  }
}
