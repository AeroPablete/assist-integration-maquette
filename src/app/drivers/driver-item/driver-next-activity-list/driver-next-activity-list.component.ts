import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-driver-next-activity-list',
  templateUrl: './driver-next-activity-list.component.html',
  styleUrls: ['./driver-next-activity-list.component.scss']
})
export class DriverNextActivityListComponent {

  /** Is Expanded Flag */
  @Input('isExpanded') isExpanded!: boolean

  /**
   * 
   */
  toggleBody() {
    this.isExpanded = !this.isExpanded
  }
}
