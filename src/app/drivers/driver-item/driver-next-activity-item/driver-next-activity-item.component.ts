import { Component } from '@angular/core';

@Component({
  selector: 'app-driver-next-activity-item',
  templateUrl: './driver-next-activity-item.component.html',
  styleUrls: ['./driver-next-activity-item.component.scss']
})
export class DriverNextActivityItemComponent {

  /** Is Expanded Flag */
  isExpanded: boolean = false

  /**
   * 
   */
  toggleAccordeon() {
    this.isExpanded = !this.isExpanded
  }
}
