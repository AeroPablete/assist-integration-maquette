import { Component, Input } from '@angular/core';
import { Driver } from 'src/app/models/driver.model';

@Component({
  selector: 'app-driver-item',
  templateUrl: './driver-item.component.html',
  styleUrls: ['./driver-item.component.scss']
})
export class DriverItemComponent {

  /** Driver */
  @Input('driver') driver!: Driver

  /** Is Expanded Flag */
  @Input('expandItem') isExpanded!: boolean;
}
