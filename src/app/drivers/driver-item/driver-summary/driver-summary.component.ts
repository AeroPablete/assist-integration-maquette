import { Component, Input } from '@angular/core';
import { Driver } from 'src/app/models/driver.model';

@Component({
  selector: 'app-driver-summary',
  templateUrl: './driver-summary.component.html',
  styleUrls: ['./driver-summary.component.scss']
})
export class DriverSummaryComponent {

  /** Driver */
  @Input('driver') driver!: Driver
}
