import { Component, Input } from '@angular/core';
import { Driver } from 'src/app/models/driver.model';

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.scss']
})
export class DriverListComponent {

  /** Driver List */
  @Input('drivers') driverList: Driver[] = []

  /** Expand Items Flag */
  @Input('expandItems') expandItems!: boolean;

}
