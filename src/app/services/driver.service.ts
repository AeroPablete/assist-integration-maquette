import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Driver } from '../models/driver.model';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  /** ... */
  private _driverListSubject = new BehaviorSubject<Driver[]>([])

  /**
   * 
   * @param data 
   */
  setData(drivers: Driver[], unassignedDrivers: Driver[]) {
    // TODO : A deplacer dans le "middle"
    drivers.forEach(elm => elm.isAssigned = true)
    unassignedDrivers.forEach(elm => elm.isAssigned = false)

    this._driverListSubject.next([...drivers, ...unassignedDrivers])
  }

  /**
   * 
   * @returns 
   */
  getData() {
    return this._driverListSubject.asObservable()
  }

  /**
   * 
   */
  getDataSnapshot() {
    return this._driverListSubject.value
  }
}
