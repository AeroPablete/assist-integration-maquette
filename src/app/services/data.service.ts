import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { OptimisationResult } from '../models/optimization-result.model';
import { DriverService } from './driver.service';
import { KpiService } from './kpi.service';
import { OrderService } from './order.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  /** ... */
  private _isLoadingDataSubject = new BehaviorSubject(false)

  /** ... */
  constructor(
    private _httpClient: HttpClient,
    private _driverService: DriverService,
    private _kpiService: KpiService,
    private _orderService: OrderService) { }

  /**
   * 
   */
  optimizeRoutes() {
    this._isLoadingDataSubject.next(true)
    const url = 'assets/data/optimization-result.json'
    
    this._httpClient
      .get<OptimisationResult>(url)
      .subscribe(data => {
        const { drivers, unassignedDrivers, unassignedOrders } = data
        
        this._driverService.setData(drivers, unassignedDrivers)
        this._kpiService.setData(drivers)
        this._orderService.setData(unassignedOrders)

        setTimeout(() => this._isLoadingDataSubject.next(false), 5000)
      })
  }

  /**
   * 
   */
  get isLoadingData() {
    return this._isLoadingDataSubject.asObservable()
  }
}
