import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Driver } from '../models/driver.model';
import { KPI } from '../models/kpi.model';

@Injectable({
  providedIn: 'root'
})
export class KpiService {

  /** ... */
  private _kpiListSubject = new BehaviorSubject<KPI[]>([])

  /**
   * 
   * @param data 
   */
  setData(data: Driver[]) {
    // TODO : Extract KPIs from optimization Data

    this._kpiListSubject.next([
      {label: 'KPI One', value: '80%'},
      {label: 'KPI Two', value: '60%'},
      {label: 'KPI Three', value: '40%'},
      {label: 'KPI Four', value: '90%'}
    ])
  }

  /**
   * 
   * @returns 
   */
  getData() {
    return this._kpiListSubject.asObservable()
  }

  /**
   * 
   */
  getDataSnapshot() {
    return this._kpiListSubject.value
  }
}
