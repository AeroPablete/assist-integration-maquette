import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Drop } from '../models/drop.model';
import { Order } from '../models/order.model';

interface OrderData {
  deliveries: {[city: string]: Order[]}
  pickups: {[city: string]: Order[]}
}

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  /** ... */
  private _orderListSubject = new BehaviorSubject<OrderData>({deliveries: {}, pickups: {}})

  /**
   * 
   * @param data 
   */
  setData(data: Order[]) {
    const deliveries = this._getDeliveries(data)
    const pickups = this._getPickups(data)

    this._orderListSubject.next({deliveries, pickups})
  }

  _getDeliveries(data: Order[]) {
    const deliveries: {[city: string]: Order[]} = {}

    data.forEach(o => {
      // Extraction des clefs
      const keys = o.drops.map(d => {
        const city = d.deliveryClient?.location?.city || ''
        const state = d.deliveryClient?.location?.stateOrProvince || ''
        const country = d.deliveryClient?.location?.country || ''
  
        return `${city}.${state}.${country}`
      })  
      const cityKeys = [...new Set(keys)]

      // Ajout de la commande dans le dictionnaire
      cityKeys.forEach(key => {
        const values = deliveries[key] || []
        values.push(o)
        deliveries[key] = values
      })
    })

    return deliveries
  }

  _getPickups(data: Order[]) {
    const pickups: {[city: string]: Order[]} = {}

    data.forEach(o => {
      // Extraction des clefs
      const keys = o.drops.map(d => {
        const city = d.pickupClient?.location?.city || ''
        const state = d.pickupClient?.location?.stateOrProvince || ''
        const country = d.pickupClient?.location?.country || ''
  
        return `${city}.${state}.${country}`
      })  
      const cityKeys = [...new Set(keys)]
      
      // Ajout de la commande dans le dictionnaire
      cityKeys.forEach(key => {
        const values = pickups[key] || []
        values.push(o)
        pickups[key] = values
      })
    })

    return pickups
  }

  /**
   * 
   * @returns 
   */
  getData() {
    return this._orderListSubject.asObservable()
  }

  /**
   * 
   */
  getDataSnapshot() {
    return this._orderListSubject.value
  }
}
